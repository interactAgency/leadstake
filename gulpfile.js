var gulp = require('gulp');
var server = require('gulp-server-livereload');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var useref = require('gulp-useref');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var cleanDest = require('gulp-clean-dest');
var ftp = require('vinyl-ftp');
var notify = require('gulp-notify');



//SERVER
gulp.task('serv', function() {
  gulp.src('./app')
    .pipe(server({
      livereload: true,
      defaultFile: 'index.html',
      open: true
    }));
});

//STYLES
gulp.task('styles', function () {
  return gulp.src('./app/sass/**/*.sass')
    .pipe(sass().on('error', sass.logError))
    .pipe(prefix({
		browsers: ['last 15 versions'],
		cascade: false
	}))
    .pipe(gulp.dest('./app/css'));
});


gulp.task('images', function () {
    return gulp.src('./app/img/**/*')
        .pipe(imagemin({
          progressive: true
        }))
        .pipe(gulp.dest('leadstake/img'));
});

//fonts
gulp.task('fonts', function () {
    return gulp.src('./app/fonts/**/*.*')
        .pipe(gulp.dest('leadstake/fonts'));
});

//mail.php replace
gulp.task('replace', function () {
    return gulp.src('./app/*.php')
        .pipe(gulp.dest('leadstake'));
});

//BUILD
gulp.task('build', ['replace'], function () {
    return gulp.src('./app/*.html')
        .pipe(cleanDest('leadstake'))
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', csso()))
        .pipe(gulp.dest('leadstake'));
});

//ftp
gulp.task('send', ['build', 'images'], function() {
    var conn = ftp.create( {
        host:     '77.120.110.166',
        user:     'alexlabs',
        password: 'Arj4h00F9x',
        parallel: 5
    } );

    /* list all files you wish to ftp in the glob variable */
    var globs = [
        'leadstake/**/*',
        '!node_modules/**' // if you wish to exclude directories, start the item with an !
    ];

    return gulp.src( globs, { base: '.', buffer: false } )
        .pipe( conn.newer( '/public_html/' ) ) // only upload newer files
        .pipe( conn.dest( '/public_html/' ) )
        .pipe(notify("Dev site updated!"));

});

//watchers
gulp.task('watch', function () {
	gulp.watch('./app/sass/**/*.sass', ['styles']);
});

gulp.task('default', ['serv', 'watch']);
