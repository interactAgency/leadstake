$(document).ready(function() {
    if( $(window).width() > 480 ){
        $('#fullpageMain').fullpage({
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: ['Главная', 'Услуги', 'Почему мы?', "Команда", "Клиенты", "Сертификаты", "Контакты"],
            showActiveTooltip: false,
            slidesNavigation: true,
            slidesNavPosition: 'bottom',
            controlArrows: false
        });
        $('#fullpageAbout').fullpage({
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: ['Миссия', 'Не работаем с', 'Команда', "Контакты"],
            showActiveTooltip: false,
            slidesNavigation: true,
            slidesNavPosition: 'bottom',
            controlArrows: false
        });
    }

    $('.collapsible').collapsible({
      accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
    });
    $(function(){
        $(".search-line").typed({
            strings: ["Котне", "Контекстная реклама"],
            typeSpeed: 0
        });
    });

    $('.fix-menuBtn').click(function(){
        if($(this).hasClass('clicked')){
            $(this).removeClass('clicked');
            $(".menu-effect").removeClass('menu-opening');
            $('.fix-logo img').attr('src', 'http://leadstake.pro/wp-content/themes/leadstake/img/logo.png');
            $('.abs-logo img').attr('src', 'http://leadstake.pro/wp-content/themes/leadstake/img/logo.png');
            $(".fix-phones a").removeClass('black-text');
            $(".menu-screen").hide()
        }else{
            $(this).addClass('clicked');
            $(".menu-effect").addClass('menu-opening');
            $('.fix-logo img').attr('src', 'http://leadstake.pro/wp-content/themes/leadstake/img/logoBlack.png');
            $('.abs-logo img').attr('src', 'http://leadstake.pro/wp-content/themes/leadstake/img/logoBlack.png');
            $(".fix-phones a").addClass('black-text');
            function showMenu(){
                $(".menu-screen").show();
            }
            setTimeout( showMenu, 300 )
        }

    })


    $('.fix-connectBtn').hover(function(){
        $('.labelConnBtn').hide()
    }, function(){
        $('.labelConnBtn').show()
    })

    $('.modal-trigger').leanModal();

    $('.gallery_s').magnificPopup({
		delegate: 'a', // child items selector, by clicking on it popup will open
		type: 'image',
		gallery:{
			enabled:true
		}
	});

    $("#callForm, #writeForm").submit(function() {
        if( $(this).find('#name').val() != '' && ( $(this).find('#phone').val() || $(this).find('#email').val() ) ){
            $.ajax({
                type: "POST",
                url: "mail.php",
                data: $(this).serialize()
            }).done(function() {
                $(this).find("input").val("");
                alert("Спасибо за заявку! Скоро мы с вами свяжемся.");
                $("#callForm, #writeForm").trigger("reset");
            });
            return false;
        }
	});

});

$(window).on('load', function() { 
  $(".loader_inner").fadeOut(); 
  $(".loader").delay(400).fadeOut("slow"); 
});